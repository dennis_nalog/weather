package com.exam.weather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exam.weather.enums.Status;
import com.exam.weather.exception.CoreServiceException;
import com.exam.weather.exception.ExceedLimitOfServiceCallException;
import com.exam.weather.model.Response;
import com.exam.weather.service.WeatherService;
import com.exam.weather.util.AppExceptionUtil;

@RestController
public class WeatherController {
    @Autowired
    @Qualifier("weatherService")
    private WeatherService weatherService;

    @GetMapping("/list")
    public Response getWeather() {
        Response response = new Response();
        try {
            response.setList(weatherService.getWeather());
            response.setStatus(Status.SUCCESS);
        } catch (CoreServiceException e) {
            response.setStatus(Status.ERROR);
            response.setMessage("Problem encountered retrieving weather information.");
            response.setDetails(AppExceptionUtil.stackTraceToString(e));
        } catch (ExceedLimitOfServiceCallException e) {
            response.setStatus(Status.ERROR);
            response.setMessage(
                    "Request not processed. " + "You've reached the maximum allowed request of our service. "
                            + "Only 1 request every 10 minutes is allowed.");
        }
        return response;
    }

    @GetMapping("/save")
    public Response saveWeatherLog() {
        Response response = new Response();

        try {
            int savedCount = weatherService.saveWeatherLog();
            response.setStatus(Status.SUCCESS);
            response.setMessage("Successfuly saved weather logs. Total number of saved logs: " + savedCount);
        } catch (CoreServiceException e) {
            response.setStatus(Status.ERROR);
            response.setMessage("Problem encountered saving weather logs.");
            response.setDetails(AppExceptionUtil.stackTraceToString(e));
        }

        return response;

    }
}
