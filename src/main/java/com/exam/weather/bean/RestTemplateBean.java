package com.exam.weather.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("classpath:resttemplate.properties")
public class RestTemplateBean {
    @Value("${rest.connection.timeout.ms}")
    private int connectionTimeout;

    @Value("${rest.read.timeout.ms}")
    private int readTimeout;

    @Bean("restTemplate")
    public RestTemplate createRestTemplate() {
        HttpComponentsClientHttpRequestFactory chrf = new HttpComponentsClientHttpRequestFactory();
        chrf.setConnectTimeout(connectionTimeout);
        chrf.setReadTimeout(readTimeout);

        RestTemplate restTemplate = new RestTemplate(chrf);

        return restTemplate;
    }
}
