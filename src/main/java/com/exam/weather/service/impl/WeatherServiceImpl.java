package com.exam.weather.service.impl;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.exam.weather.exception.CoreServiceException;
import com.exam.weather.exception.ExceedLimitOfServiceCallException;
import com.exam.weather.model.Weather;
import com.exam.weather.model.weatherapi.WeatherAPIResponse;
import com.exam.weather.model.weatherapi.WeatherInfo;
import com.exam.weather.repository.WeatherRepository;
import com.exam.weather.service.WeatherService;
import com.exam.weather.util.AppCollectionUtil;
import com.exam.weather.util.IdUtil;

@Service("weatherService")
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    @Autowired
    private WeatherRepository weatherRepository;

    @Value("${weather.api.url}")
    private String weatherAPIUrl;

    @Value("${weather.city.id.list}")
    private String commaDelimitedCityIDList;

    @Value("${weather.max.stored.response}")
    private int maxStoredResponse;

    @Value("${weather.request.limit}")
    private int requestLimit;

    @Value("${weather.request.limit.duration}")
    private long requestLimitDurationInMillis;

    private long lastRequestedProcessedTimeMillis;

    private int numberOfRequestProcessed;

    private ArrayDeque<List<Weather>> recentResponseCache;

    @Override
    public List<Weather> getWeather() throws CoreServiceException, ExceedLimitOfServiceCallException {
        List<Weather> weatherList = new ArrayList<>();
        String id = IdUtil.generateID();
        try {
            if (isWithinRequestLimit()) {

                String requestUrl = weatherAPIUrl.replace("{id_list_param}", commaDelimitedCityIDList);
                RequestEntity<Void> request = RequestEntity.get(new URI(requestUrl)).accept(MediaType.APPLICATION_JSON)
                        .build();
                ResponseEntity<WeatherAPIResponse> responseEntity = restTemplate.exchange(request,
                        WeatherAPIResponse.class);

                if (HttpURLConnection.HTTP_OK == responseEntity.getStatusCodeValue()) {
                    WeatherAPIResponse weatherAPIResponse = responseEntity.getBody();
                    for (WeatherInfo weatherInfo : weatherAPIResponse.getList()) {
                        Weather weather = new Weather(id, weatherInfo.getName(), weatherInfo.getActualWeather(),
                                weatherInfo.getTemperature());

                        weatherList.add(weather);
                    }

                    updateLastRequestProcessedTimeStamp();
                    updateRecentResponseCache(weatherList);
                    updateNumberOfRequestProcessed();
                } else {
                    throw new CoreServiceException("Problem encountered in weather API. Status Code "
                            + responseEntity.getStatusCode() + ". \n");
                }
            } else {
                throw new ExceedLimitOfServiceCallException();
            }
        } catch (RestClientException | URISyntaxException e) {
            throw new CoreServiceException(e);
        }

        return weatherList;
    }

    private void updateRecentResponseCache(List<Weather> weatherList) {
        if (recentResponseCache == null) {
            recentResponseCache = new ArrayDeque<>(maxStoredResponse);
        }

        if (recentResponseCache.size() == maxStoredResponse) {
            recentResponseCache.removeLast();
        }
        recentResponseCache.addFirst(weatherList);
    }

    private void updateNumberOfRequestProcessed() {
        numberOfRequestProcessed = numberOfRequestProcessed + 1;
    }

    private void updateLastRequestProcessedTimeStamp() {
        lastRequestedProcessedTimeMillis = System.currentTimeMillis();
    }

    private boolean isWithinRequestLimit() {
        boolean isAllowed = true;

        if (!(isRequestWithinTheRequestLimitDuration())) {
            resetVariablesForMonitoringRequestLimit();
        }

        if (numberOfRequestProcessed >= requestLimit && isRequestWithinTheRequestLimitDuration()) {
            isAllowed = false;
        }

        return isAllowed;
    }

    private boolean isRequestWithinTheRequestLimitDuration() {
        boolean isWithinTheRequestLimitDuration = true;

        long differenceOfCurrentTimeAndLastRequestProcessTimeInMillis = System.currentTimeMillis()
                - lastRequestedProcessedTimeMillis;

        if (differenceOfCurrentTimeAndLastRequestProcessTimeInMillis > requestLimitDurationInMillis) {
            isWithinTheRequestLimitDuration = false;
        }

        return isWithinTheRequestLimitDuration;
    }

    private void resetVariablesForMonitoringRequestLimit() {
        numberOfRequestProcessed = 0;
    }

    @Override
    public int saveWeatherLog() throws CoreServiceException {
        int savedResponseCount = 0;

        if (AppCollectionUtil.isNotEmpty(recentResponseCache)) {
            long timestamp = Instant.now().toEpochMilli();

            for (List<Weather> list : recentResponseCache) {

                if (AppCollectionUtil.isNotEmpty(recentResponseCache)) {
                    for (Weather weather : list) {
                        weather.setDtimeInserted(timestamp);
                    }

                    try {
                        weatherRepository.saveAll(list);
                    } catch (DataAccessException e) {
                        throw new CoreServiceException(e);
                    }
                }
            }
            savedResponseCount = recentResponseCache.size();
            recentResponseCache.clear();
        }
        return savedResponseCount;
    }

}
