package com.exam.weather.service;

import java.util.List;

import com.exam.weather.exception.CoreServiceException;
import com.exam.weather.exception.ExceedLimitOfServiceCallException;
import com.exam.weather.model.Weather;

public interface WeatherService {
    List<Weather> getWeather() throws CoreServiceException, ExceedLimitOfServiceCallException;

    int saveWeatherLog() throws CoreServiceException;
}
