package com.exam.weather.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.exam.weather.model.Weather;

@Repository
public interface WeatherRepository extends CrudRepository<Weather, Long> {

}
