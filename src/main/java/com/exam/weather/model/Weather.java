package com.exam.weather.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "WeatherLog")
public class Weather {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;

    @Column
    @JsonIgnore
    private String responseId;

    @Column
    private String location;

    @Column
    private String actualWeather;

    @Column
    private String temperature;
    @Column
    @JsonIgnore
    private long dtimeInserted;

    public Weather() {
    }

    public Weather(String responseId, String location, String actualWeather, String temperature) {
        this.responseId = responseId;
        this.location = location;
        this.actualWeather = actualWeather;
        this.temperature = temperature;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getActualWeather() {
        return actualWeather;
    }

    public void setActualWeather(String actualWeather) {
        this.actualWeather = actualWeather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public long getDtimeInserted() {
        return dtimeInserted;
    }

    public void setDtimeInserted(long dtimeInserted) {
        this.dtimeInserted = dtimeInserted;
    }

}
