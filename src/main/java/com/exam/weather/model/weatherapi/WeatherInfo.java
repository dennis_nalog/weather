package com.exam.weather.model.weatherapi;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherInfo {
    private String name;
    private List<Weather> weather;
    private Main main;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public String getActualWeather() {
        String actualWeather = null;
        if (weather != null && weather.size() >= 1) {
            actualWeather = weather.get(0).getDescription();
        }
        return actualWeather;
    }

    public String getTemperature() {
        String temperature = null;
        if (main != null) {
            temperature = main.getTemp();
        }
        return temperature;
    }

}
