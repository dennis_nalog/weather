package com.exam.weather.model.weatherapi;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherAPIResponse {
    List<WeatherInfo> list;

    public List<WeatherInfo> getList() {
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public void setList(List<WeatherInfo> list) {
        this.list = list;
    }

}
