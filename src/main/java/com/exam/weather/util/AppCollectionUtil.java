package com.exam.weather.util;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;

public class AppCollectionUtil {

    public static boolean isNotEmpty(Collection<?> collection) {
        return CollectionUtils.isNotEmpty(collection);
    }
}
