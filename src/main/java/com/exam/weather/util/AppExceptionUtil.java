package com.exam.weather.util;

import org.apache.commons.lang.exception.ExceptionUtils;

public class AppExceptionUtil {
    public static String stackTraceToString(Throwable throwable) {
        return ExceptionUtils.getStackTrace(throwable);
    }
}
