package com.exam.weather.exception;

public class CoreServiceException extends Exception {

    private static final long serialVersionUID = 7258370942790934244L;

    public CoreServiceException(Throwable arg0) {
        super(arg0);
    }

    public CoreServiceException(String arg0) {
        super(arg0);
    }

}
