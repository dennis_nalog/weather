package com.exam.weather.exception;

public class CoreDaoException extends Exception {

    private static final long serialVersionUID = -2101072040180775892L;

    public CoreDaoException(Throwable arg0) {
        super(arg0);
    }

}
