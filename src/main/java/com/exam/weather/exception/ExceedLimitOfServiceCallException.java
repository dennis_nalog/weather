package com.exam.weather.exception;

public class ExceedLimitOfServiceCallException extends Exception {

    private static final long serialVersionUID = 625680794521337965L;

    public ExceedLimitOfServiceCallException() {
        super();
    }

    public ExceedLimitOfServiceCallException(String arg0) {
        super(arg0);
    }

}
